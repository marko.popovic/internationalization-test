import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test-app';
  clickCounter = 0;
  gender = 'male'
  incrementCounter(i: number) {
    this.clickCounter = this.clickCounter + i;

  }
  setGender(gender: string) {
    this.gender = gender
  }
}
