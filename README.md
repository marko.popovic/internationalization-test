# TestApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8.

## Development server

Run `npm install`.
Run `start:app` for a dev server. Default browser will open on `http://localhost:4200/` and page will be in `English`.
Run `app:start:with-lang:sr` for a dev server. Default browser will open on `http://localhost:4200/` and page will be in `Serbian`.
Run `app:start:with-lang:en` for a dev server. Default browser will open on `http://localhost:4200/` and page will be in `English`.
